package deserializer;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import api.Book;

public class BookDeserializer implements JsonDeserializer<Book> {

	public Book deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		JsonObject jObject = json.getAsJsonObject();
		int id = jObject.get("ID").getAsInt();
		String title = jObject.get("Title").getAsString();
		String description = jObject.get("Description").getAsString();
		int pageCount = jObject.get("PageCount").getAsInt();
		String excerpt = jObject.get("Excerpt").getAsString();
		String publishDate = jObject.get("PublishDate").getAsString();
		
		return new Book(id, title, description, pageCount, excerpt, publishDate);
	}
}