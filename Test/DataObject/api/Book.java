package api;

import com.google.gson.GsonBuilder;
import com.logigear.utility.Utility;
import deserializer.BookDeserializer;

public class Book {

	private int id;
	private String title;
	private String description;
	private int pageCount;
	private String excerpt;
	private String publishDate;

	public Book() {
	}

	public Book(int id, String title, String description, int pageCount, String excerpt, String publishDate) {
		this.id = id;
		this.title = title;
		this.description = description;
		this.pageCount = pageCount;
		this.excerpt = excerpt;
		this.publishDate = publishDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public String getExcerpt() {
		return excerpt;
	}

	public void setExcerpt(String excerpt) {
		this.excerpt = excerpt;
	}

	public String getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}

	public void initData() {
		this.id = Utility.generateRandomNumber();
		this.title = Utility.generateRandomString(10);
		this.description = Utility.generateRandomString(100);
		this.pageCount = Utility.generateRandomNumber();
		this.excerpt = Utility.generateRandomString(200);
		this.publishDate = Utility.getCurrentDate();
		
//		this.publishDate = "2018-11-15T23:19:08";
	}
	
	public static Book deserialize(String json)
	{
		GsonBuilder gsonBldr = new GsonBuilder();
	    gsonBldr.registerTypeAdapter(Book.class, new BookDeserializer());
	    return gsonBldr.create().fromJson(json, Book.class);
	}
}
