package heroku_app;

import com.loginium.driver.DriverManager;
import com.loginium.element.Element;

public class JavaScriptAlertsPage {

	private Element btnClickForJSAlert = new Element("//button[text()='Click for JS Alert']");
	private Element btnClickForJSConfirm = new Element("//button[text()='Click for JS Confirm']");
	private Element btnClickForJSPrompt = new Element("//button[text()='Click for JS Prompt']");
	private Element lblResult = new Element("//p[@id='result']");

	public JavaScriptAlertsPage triggerJSAlert() {
		this.btnClickForJSAlert.click();
		DriverManager.getDriver().switchToAlert().accept();
		return this;
	}

	public JavaScriptAlertsPage triggerJSAlertConfirm(boolean accept) {
		this.btnClickForJSConfirm.click();
		handleAlert(accept);

		return this;
	}

	public JavaScriptAlertsPage triggerJSAlertPrompt(String input, boolean accept) {
		this.btnClickForJSPrompt.click();
		DriverManager.getDriver().switchToAlert().sendKeys(input);
		handleAlert(accept);

		return this;
	}

	private void handleAlert(boolean accept) {
		if (accept)
			DriverManager.getDriver().switchToAlert().accept();
		else
			DriverManager.getDriver().switchToAlert().dismiss();
	}

	public String getMessage() {
		return this.lblResult.getText();
	}

}
