package heroku_app;

import com.loginium.driver.DriverManager;
import com.loginium.element.Element;

public class HomePage {

	private Element lnkJSAlerts = new Element("//a[@href='/javascript_alerts']");

	public JavaScriptAlertsPage gotoJavaScriptAlertsPage() {
		this.lnkJSAlerts.click();
		return new JavaScriptAlertsPage();
	}

	public HomePage open() {
		DriverManager.getDriver().get("https://the-internet.herokuapp.com/");
		return this;
	}

}
