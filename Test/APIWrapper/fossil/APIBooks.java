package fossil;

import com.loginium.api.APIRequest;
import com.loginium.api.APIResponse;

import api.Book;

public class APIBooks extends APIBase {

	public static APIResponse getBooks() {
		APIRequest request = new APIRequest();
		request.setURL(baseURL + "/api/Books");
		request.addHeader("Accept", "application/json");
		return request.sendGETRequest();
	}

	public static APIResponse postBook(Book book) {
		APIRequest request = new APIRequest();
		request.setURL(baseURL + "/api/Books");
		request.addHeader("Content-Type", "application/json");
		request.setBody(book);
		return request.sendPOSTRequest();
	}

}
