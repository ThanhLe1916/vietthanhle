package fossil;

import com.loginium.api.APIRequest;
import com.loginium.api.APIResponse;

public class APIActivities extends APIBase{
	// https://fakerestapi.azurewebsites.net/swagger/ui/index#/Activities

	// GET /api/Activities
	public static APIResponse getActivities() {
		
		APIRequest request = new APIRequest();
		request.setURL(baseURL + "/api/Activities");
		request.addHeader("Accept", "application/json");
		return request.sendGETRequest();
	}
	
	public static APIResponse getActivitiesByID(int id) {
		APIRequest request = new APIRequest();
		request.setURL(baseURL + "/api/Activities/" + id);
		request.addHeader("Accept", "application/json");
		return request.sendGETRequest();
	}
	

}
