package ui;

import org.testng.annotations.Test;

import heroku_app.HomePage;
import heroku_app.JavaScriptAlertsPage;

import org.testng.Assert;

public class UITest extends TestBase {

	HomePage homePage;
	JavaScriptAlertsPage jsAlertPage;

	@Test
	public void Test_JS_Alert() {
		homePage = new HomePage();
		jsAlertPage = homePage.open().gotoJavaScriptAlertsPage().triggerJSAlert();

		Assert.assertEquals(jsAlertPage.getMessage(), "You successfuly clicked an alert",
				"Expected message is not displayed.");
	}

	@Test
	public void Test_JS_Alert_Confirm_Cancel() {
		homePage = new HomePage();
		jsAlertPage = homePage.open().gotoJavaScriptAlertsPage().triggerJSAlertConfirm(false);

		Assert.assertEquals(jsAlertPage.getMessage(), "You clicked: Cancel", "Expected message is not displayed.");
	}

	@Test
	public void Test_JS_Alert_Prompt() {
		homePage = new HomePage();
		String input = "Thanh Le";
		jsAlertPage = homePage.open().gotoJavaScriptAlertsPage().triggerJSAlertPrompt(input, true);

		Assert.assertEquals(jsAlertPage.getMessage(), "You entered: " + input + ".",
				"Expected message is not displayed.");
	}

}
