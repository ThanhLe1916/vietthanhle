package ui;

import java.net.MalformedURLException;
import java.rmi.UnexpectedException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;

import com.loginium.driver.DriverManager;
import com.loginium.driver.DriverSetting;
import com.loginium.testng.LoginiumTestListener;

@Listeners(LoginiumTestListener.class)
public class TestBase {

	private static ApplicationContext context;
	protected DriverSetting driverSetting;

	@BeforeTest
	public void BeforeTest() {
		context = new ClassPathXmlApplicationContext("BrowserSetting.xml");
	}

	@Parameters({ "driverSettingID" })
	@BeforeMethod
	public void beforeMethod(String driverSettingID) throws MalformedURLException, UnexpectedException {
		driverSetting = (DriverSetting) context.getBean(driverSettingID);
		DriverManager.createWebDriver(driverSetting);
		DriverManager.getDriver().maximize();
	}

	@AfterMethod
	public void afterMethod(ITestResult tr) {
		DriverManager.getDriver().quit();
	}

}
