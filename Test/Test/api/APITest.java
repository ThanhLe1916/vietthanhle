package api;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.logigear.utility.Utility;
import com.loginium.api.APIResponse;

import fossil.APIActivities;
import fossil.APIBooks;

public class APITest extends APITestBase {

	@Test
	public void Get_Test() throws IOException {
		APIResponse response = APIActivities.getActivities();
		Assert.assertEquals(response.getStatus(), 200);
		Assert.assertEquals(response.queryBody("/0/ID"), "1", "Frist Activity's ID is not equal to 1");
	}

	@Test
	public void Post_Test() throws IOException {
		Book book = new Book();
		book.initData();

		APIResponse response = APIBooks.postBook(book);
		Assert.assertEquals(response.getStatus(), 200);

		Book actual = Book.deserialize(response.getBody());
		String actualStr = Utility.convertObjectToString(actual);
		String expectedStr = Utility.convertObjectToString(book);
		Assert.assertEquals(actualStr, expectedStr, "Returned Book is not as expected.");
	}

}
