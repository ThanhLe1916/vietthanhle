package api;

import java.net.MalformedURLException;
import java.rmi.UnexpectedException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import com.loginium.testng.LoginiumTestListener;

@Listeners(LoginiumTestListener.class)
public class APITestBase {

	@BeforeTest
	public void BeforeTest() {
	}

	@BeforeMethod
	public void beforeMethod() throws MalformedURLException, UnexpectedException {
	}

	@AfterMethod
	public void afterMethod(ITestResult tr) {
	}

}
