package com.logigear.utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.lang3.RandomStringUtils;

import com.google.gson.Gson;

public class Utility {

	public static int generateRandomNumber() {
		return ThreadLocalRandom.current().nextInt(1, 999999);
	}
	
	public static String generateRandomString(int length) {
		return RandomStringUtils.randomAlphanumeric(length);
	}
	
	public static String getCurrentDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());
	}
	
	public static String convertObjectToString(Object object) {
		Gson gson = new Gson();
		return gson.toJson(object);
	}
}
