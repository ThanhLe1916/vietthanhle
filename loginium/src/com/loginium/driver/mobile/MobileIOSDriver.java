package com.loginium.driver.mobile;

import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.loginium.LoginiumConstants.Cloud;
import com.loginium.driver.DriverBase;
import com.loginium.driver.DriverSetting;
import io.appium.java_client.remote.MobileCapabilityType;

public class MobileIOSDriver extends DriverBase {

	public MobileIOSDriver(DriverSetting driverSetting) throws MalformedURLException {
		super(driverSetting);

		DesiredCapabilities capabilities = new DesiredCapabilities();
		String url = driverSetting.getRemoteURL();

		if (driverSetting.getBrowserOnTheCloud() == Cloud.BROWSER_STACK) {
			capabilities.setCapability("browserName", driverSetting.getBrowserName().toString().toUpperCase());
			capabilities.setCapability("device", driverSetting.getDeviceName());
			capabilities.setCapability("realMobile", "true");
			capabilities.setCapability("os_version", driverSetting.getPlatformVersion());
			capabilities.setCapability("browserstack.debug", true);

			url = "https://" + driverSetting.getBrowserStackUsername() + ":" + driverSetting.getBrowserStackPassword()
					+ "@hub-cloud.browserstack.com/wd/hub";
		} else if (driverSetting.getBrowserOnTheCloud() == Cloud.SAUCE_LABS) {
			throw new UnsupportedOperationException("Doesn't support Sauce Labs yet.");
		} else {
			capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "MAC");
			capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, driverSetting.getPlatformVersion());
			capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, driverSetting.getDeviceName());
			capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "Safari");
			capabilities.setCapability("safariInitialUrl", "http://www.google.com");
			url = driverSetting.getRemoteURL();
		}

		setWebDriver(new RemoteWebDriver(new URL(url), capabilities));
	}

	public void maximize() {
		// Do nothing on mobile devices
	}
}
