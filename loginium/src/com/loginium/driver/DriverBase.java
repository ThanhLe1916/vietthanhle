package com.loginium.driver;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.util.StopWatch;

import com.loginium.element.Element;
import com.loginium.element.Locator;

public class DriverBase implements Driver {

	protected static Logger _log = LogManager.getLogger();
	protected WebDriver _webDriver;
	protected DriverSetting _driverSetting;
	protected int _elementTimeout;

	public WebDriver getWebDriver() {
		return _webDriver;
	}

	protected void setWebDriver(WebDriver webDriver) {
		this._webDriver = webDriver;
	}

	public DriverSetting getDriverSetting() {
		return _driverSetting;
	}

	protected void setDriverSetting(DriverSetting driverSetting) {
		this._driverSetting = driverSetting;
	}

	public DriverBase(DriverSetting driverSetting) {
		setDriverSetting(driverSetting);
		this._elementTimeout = driverSetting.getElementWaitTimeOut();
	}

	public void get(String url) {
		try {
			this._webDriver.get(url);
			logEndAction("Got to " + url);
		} catch (Exception ex) {
			_log.info(ex.getStackTrace());
		}
	}

	public List<Element> findElements(Locator locator) {

		List<Element> elements = new ArrayList<Element>();

		StopWatch sw = new StopWatch();
		sw.start();

		try {
			WebDriverWait driverWait = new WebDriverWait(this._webDriver, this._driverSetting.getElementWaitTimeOut());
			List<WebElement> webElements = driverWait
					.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator.getBy()));

			int i = 0;
			for (WebElement webElement : webElements) {
				elements.add(new Element(locator, webElement, i));
				i++;
			}
		} catch (Exception ex) {

		} finally {
			sw.stop();
		}
		return elements;
	}

	public void close() {
		try {
			this._webDriver.close();
			logEndAction("Closed window");
		} catch (Exception ex) {
			_log.info(ex.getStackTrace());
		}
	}

	public void quit() {
		try {
			this._webDriver.close();
			logEndAction("quit browser");
		} catch (Exception ex) {
			_log.info(ex.getStackTrace());
		}
	}

	public AlertBase switchToAlert() {
		StopWatch sw = new StopWatch();
		sw.start();

		try {
			WebDriverWait driverWait = new WebDriverWait(this._webDriver, this._driverSetting.getElementWaitTimeOut());

			logEndAction("Switched to Alert");
			return new AlertBase(driverWait.until(ExpectedConditions.alertIsPresent()));
		} catch (Exception ex) {
			_log.info(ex.getStackTrace());
		} finally {
			sw.stop();
		}
		
		return null;
	}

	public void maximize() {
		this._webDriver.manage().window().maximize();
		logEndAction("Maximized window");
	}
	
	private void logEndAction(String msg) {
		_log.info("Done: [{}]", msg);
	}

	
}
