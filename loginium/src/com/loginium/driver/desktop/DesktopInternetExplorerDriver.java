package com.loginium.driver.desktop;

import java.net.MalformedURLException;

import com.loginium.driver.DriverBase;
import com.loginium.driver.DriverSetting;

public class DesktopInternetExplorerDriver extends DriverBase {

	public DesktopInternetExplorerDriver(DriverSetting driverSetting) throws MalformedURLException {
		super(driverSetting);
		
		throw new UnsupportedOperationException("Doesn't support Internet Explorer yet.");
	}
}
