package com.loginium.driver.desktop;

import com.loginium.driver.DriverBase;
import com.loginium.driver.DriverSetting;

public class DesktopFireFoxDriver extends DriverBase {

	public DesktopFireFoxDriver(DriverSetting driverSetting) {
		super(driverSetting);

		throw new UnsupportedOperationException("Doesn't support Firefox yet.");
	}
}
