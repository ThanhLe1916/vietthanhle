package com.loginium.driver;

import org.openqa.selenium.WebDriver;

public interface Driver {
  WebDriver getWebDriver();
  DriverSetting getDriverSetting();  
  AlertBase switchToAlert();
  
  void maximize();
  void quit();
  void close();
  void get(String url);
}
