package com.loginium.driver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Alert;;

public class AlertBase implements Alert {

	protected static Logger log = LogManager.getLogger();
	protected Alert _alert;

	public AlertBase(Alert alert) {
		this._alert = alert;
	}

	public void dismiss() {
		this._alert.dismiss();
		logEndAction("JS Alert dismissed");
	}

	public void accept() {
		this._alert.accept();
		logEndAction("JS Alert accepted");
	}

	public String getText() {
		logStartAction("Getting text of JS Alert");
		return this._alert.getText();
	}

	public void sendKeys(String keysToSend) {
		this._alert.sendKeys(keysToSend);
		logEndAction("Sent values to JS Alert");
	}

	 private void logStartAction(String msg) {
	 log.info("Executing: [{}]", msg);
	 }

	private void logEndAction(String msg) {
		log.info("Done: [{}]", msg);
	}

}
