package com.loginium.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class APIResponse {

	private int _status;
	private String _body;

	public APIResponse(int responseCode, String body) {
		this._status = responseCode;
		this._body = body;
	}

	public int getStatus() {
		return this._status;
	}

	public String getBody() {
		return this._body;
	}

	// using xpath-like string to query
	// Creates a JSONPointer using an initialization string and tries to match it to
	// an item within this JSONArray. For example, given a JSONArray initialized
	// with this document:
	// [
	// {"b":"c"}
	// ]
	//
	// and this JSONPointer string: "/0/b"
	// Creates a JSONPointer using an initialization string and tries to match it to
	// an item within this JSONObject. For example, given a JSONObject initialized
	// with this document:
	// {
	// "a":{"b":"c"}
	// }
	//
	// and this JSONPointer string: "/a/b"
	public String queryBody(String query) {

		if (this._body.startsWith("[")) {
			JSONArray json = new JSONArray(this._body);
			return json.query(query).toString();
		} else if (this._body.startsWith("{")) {
			JSONObject json = new JSONObject(this._body);
			return json.query(query).toString();
		} else {
			throw new JSONException("Invalid Json String.");
		}

	}
}
