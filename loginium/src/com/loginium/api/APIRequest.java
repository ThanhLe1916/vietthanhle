package com.loginium.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

public class APIRequest {

	protected static Logger _log = LogManager.getLogger();
	private String _url;
	private Map<String, Object> _headers = new HashMap<String, Object>();
	private String _bodyJSON;
	private Map<String, Object> _params = new HashMap<String, Object>();

	public void setURL(String url) {
		this._url = url;
	}

	public void addHeader(String key, String value) {
		addHeader(key, (Object) value);
	}

	public void addHeader(String key, int value) {
		addHeader(key, (Object) value);
	}

	public void addHeader(String key, boolean value) {
		addHeader(key, (Object) value);
	}

	public void addHeader(String key, Object value) {
		this._headers.put(key, value);
	}

	public void setBody(String json) {
		this._bodyJSON = json;
	}

	public void setBody(Object object) {
		JSONObject json = new JSONObject(object);
		this._bodyJSON = json.toString();
	}

	public void addParam(String key, Object value) {
		this._params.put(key, value);
	}

	private HttpGet createGETRequest() {

		HttpGet request = new HttpGet(this._url);

		for (Map.Entry<String, Object> entry : this._headers.entrySet()) {
			request.setHeader(entry.getKey(), entry.getValue().toString());
		}

		_log.info("Creating GET request. " + request.toString());
		return request;
	}

	private HttpPost createPOSTRequest() {

		HttpPost request = new HttpPost(this._url);

		for (Map.Entry<String, Object> entry : this._headers.entrySet()) {
			request.setHeader(entry.getKey(), entry.getValue().toString());
		}

		if (this._bodyJSON != null) {
			StringEntity entity = new StringEntity(this._bodyJSON, "UTF-8");
			entity.setContentType("application/json");
			request.setEntity(entity);
		}
		_log.info("Creating POST request. " + request.toString());
		return request;
	}

	private APIResponse extractResponse(HttpResponse httpResponse) {
		HttpEntity entity = httpResponse.getEntity();
		BufferedReader br = null;
		String entireResponse = new String();

		try {
			br = new BufferedReader(new InputStreamReader(entity.getContent()));
			String output;
			while ((output = br.readLine()) != null) {
				entireResponse += output;
			}
		} catch (UnsupportedOperationException e) {
			_log.info("Error when extracting API response. Error: " + e.getStackTrace());
		} catch (IOException e) {
			_log.info("Error when extracting API response. Error: " + e.getStackTrace());
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				_log.info("Error when extracting API response. Error: " + e.getStackTrace());
			}
		}

		return new APIResponse(httpResponse.getStatusLine().getStatusCode(), entireResponse);
	}

	public APIResponse sendGETRequest() {
		
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();

		try {
			return this.extractResponse(httpClient.execute(this.createGETRequest()));
		} catch (Exception ex) {
			_log.info("Error When sending GET request. Error: " + ex.getStackTrace());
		} finally {
			try {
				httpClient.close();
			} catch (IOException e) {
				_log.info("Error while closing httpClient.");
			}
		}
		return null;
	}

	public APIResponse sendPOSTRequest() {
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();

		try {
			return this.extractResponse(httpClient.execute(this.createPOSTRequest()));
		} catch (Exception e) {
			_log.info("Error When sending POST request. Error: " + e.getStackTrace());
		} finally {
			try {
				httpClient.close();
			} catch (IOException e) {
				_log.info("Error while closing httpClient.");
			}
		}
		return null;
	}
}
