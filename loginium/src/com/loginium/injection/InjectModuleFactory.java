package com.loginium.injection;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.IModuleFactory;
import org.testng.ITestContext;

import com.google.inject.Module;

public class InjectModuleFactory implements IModuleFactory {

  private static ApplicationContext springContext;
  protected static Logger log = LogManager.getLogger();

  public Module createModule(ITestContext context, Class<?> testClass) {
    springContext = new ClassPathXmlApplicationContext("ClassMappingList.xml");
    String classSuffix = context.getCurrentXmlTest().getParameter("language");
    @SuppressWarnings("unchecked")
    List<String> classesList = ((List<String>) springContext.getBean("classMappingList"));
    return new InjectModule(classSuffix, classesList);
  }
}
