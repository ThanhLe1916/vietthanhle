package com.loginium.injection;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.inject.AbstractModule;
import com.google.inject.CreationException;

public class InjectModule extends AbstractModule {

  protected static Logger log = LogManager.getLogger();
  private List<String> classMap;
  private String classSuffix;

  public InjectModule(String classSuffix, List<String> classMap) {
    this.classSuffix = classSuffix;
    this.classMap = classMap;
  }

  @SuppressWarnings({"rawtypes", "unchecked"})
  private void bindClass(String parentClassName) throws ClassNotFoundException, IOException {
    Class parentClass = Class.forName(parentClassName);
    Class<?>[] allClasses = getClasses(parentClass.getPackage().getName().split("\\.")[0]);

    String childClassName = (classSuffix.trim().equals("")) ? parentClass.getSimpleName() + "_EN"
        : parentClass.getSimpleName() + "_" + classSuffix;

    Class childClass = null;

    for (Class<?> cls : allClasses) {
      if (cls.getSimpleName().equals(childClassName)) {
        childClass = cls;
        break;
      }
    }

    try {
      if (childClass == null)
        throw new ClassNotFoundException();
      else {
        bind(parentClass).to(childClass);
        log.info(
            String.format("Class %s was bound to class %s", parentClassName, childClass.getName()));
      }
    } catch (CreationException ex) {
      log.info(ex.getMessage());
    }

  }

  private Class<?>[] getClasses(String packageName) throws ClassNotFoundException, IOException {

    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

    assert classLoader != null;
    String path = packageName.replace('.', '/');
    Enumeration<URL> resources = classLoader.getResources(path);
    List<File> dirs = new ArrayList<File>();
    while (resources.hasMoreElements()) {
      URL resource = resources.nextElement();
      dirs.add(new File(resource.getFile()));
    }

    ArrayList<Class<?>> classes = new ArrayList<Class<?>>();
    for (File directory : dirs) {
      classes.addAll(findClasses(directory, packageName));
    }

    return classes.toArray(new Class[classes.size()]);

  }

  private List<Class<?>> findClasses(File directory, String packageName)
      throws ClassNotFoundException {
    List<Class<?>> classes = new ArrayList<Class<?>>();
    if (!directory.exists()) {
      return classes;
    }

    File[] files = directory.listFiles();

    for (File file : files) {
      if (file.isDirectory()) {
        assert !file.getName().contains(".");
        classes.addAll(findClasses(file, packageName + "." + file.getName()));
      } else if (file.getName().endsWith(".class")) {
        classes.add(Class
            .forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
      }
    }
    return classes;

  }

  @Override
  protected void configure() {
    for (String className : classMap) {
      try {
        bindClass(className);
      } catch (ClassNotFoundException e) {
        log.info(className + " NOT FOUND!!!");
        //TODO: No implementation for <clasName> was bound.
      } catch (Exception e) {
        log.info(e.getMessage());
      }
    }
  }
}
