package com.loginium.element;

//import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.util.StopWatch;

import com.loginium.LoginiumConstants.LocatorType;
import com.loginium.driver.DriverManager;

public class Element {

	private static Logger _log = LogManager.getLogger();
	private final Locator _locator;
	private final int _timeoutInSeconds = DriverManager.getDriver().getDriverSetting().getElementWaitTimeOut();
	private WebElement _element = null;
	private int _indexInList = -1;

	public Locator getLocator() {
		return _locator;
	}

	public Element(Locator locator) {
		this._locator = locator;
	}

	public Element(LocatorType type, String value) {
		this._locator = new Locator(type, value);
	}

	public Element(String xpath) {
		this._locator = new Locator(LocatorType.XPATH, xpath);
	}

	public Element(Locator locator, WebElement element, int indexInList) {
		this._locator = locator;
		this._element = element;
		this._indexInList = indexInList;
	}

	private WebElement getElement() {
		if (this._element == null) {
			return this.waitForElement()._element;
		}
		return this._element;
	}

	public Element waitForElement(int timeoutInSeconds) {
		StopWatch sw = new StopWatch();
		sw.start();

		// while (timeoutInSeconds > 0) {
		try {
			WebDriverWait driverWait = new WebDriverWait(DriverManager.getDriver().getWebDriver(), timeoutInSeconds);
			this._element = driverWait.until(ExpectedConditions.elementToBeClickable(this._locator.getBy()));

		} catch (TimeoutException ex) {
			logEndAction(String.format("can not find element: %s", this._locator.getBy()));
			this._element = null;
		} catch (Exception ex) {
			_log.debug("Unhandle exception: " + ex.getMessage());
			this.waitForElement(timeoutInSeconds - (int) sw.getTotalTimeSeconds());
		} finally {
			sw.stop();
		}
		return this;
	}

	public Element waitForElement() {
		return this.waitForElement(this._timeoutInSeconds);
	}

	public Element click(int timeoutInSeconds) {
		StopWatch sw = new StopWatch();
		sw.start();

		try {
			this.getElement().click();
		} catch (StaleElementReferenceException staleEx) {
			this.waitForElement(timeoutInSeconds - (int) sw.getTotalTimeSeconds())
					.click(timeoutInSeconds - (int) sw.getTotalTimeSeconds());
		} catch (Exception ex) {
			_log.info(ex.getStackTrace());
		} finally {
			sw.stop();
		}

		logEndAction(String.format("clicked: %s", this._locator.getBy()));
		return this;
	}

	public Element click() {
		return this.click(this._timeoutInSeconds);
	}

	public Element clear(int timeoutInSeconds) {
		StopWatch sw = new StopWatch();
		sw.start();

		try {
			this.getElement().clear();
		} catch (StaleElementReferenceException staleEx) {
			this.waitForElement(timeoutInSeconds - (int) sw.getTotalTimeSeconds())
					.clear(timeoutInSeconds - (int) sw.getTotalTimeSeconds());
		} catch (Exception ex) {
			_log.info(ex.getStackTrace());
		} finally {
			sw.stop();
		}

		logEndAction(String.format("cleared: %s", this._locator.getBy()));
		return this;
	}

	public Element clear() {
		return this.clear(this._timeoutInSeconds);
	}

	public Element type(boolean cleanUp, int timeoutInSeconds, CharSequence... keysToSend) {
		StopWatch sw = new StopWatch();
		sw.start();

		try {
			if (cleanUp) {
				this.getElement().clear();
			}

			this.getElement().sendKeys(keysToSend);

		} catch (StaleElementReferenceException staleEx) {
			this.waitForElement(timeoutInSeconds - (int) sw.getTotalTimeSeconds()).type(cleanUp,
					timeoutInSeconds - (int) sw.getTotalTimeSeconds(), keysToSend);
		} catch (Exception ex) {
			_log.info(ex.getStackTrace());
		} finally {
			sw.stop();
		}

		logEndAction(String.format("typed to: %s", this._locator.getBy()));
		return this;
	}

	public Element type(boolean cleanUp, CharSequence... keysToSend) {
		return this.type(cleanUp, this._timeoutInSeconds, keysToSend);
	}

	// public <X> X getScreenshotAs(OutputType<X> target) throws WebDriverException
	// {
	// return getWebElement().getScreenshotAs(target);
	// }

	// public String getTagName() {
	// logStartAction(String.format("get Tag Name: %s", locator.getBy()));
	// logEndAction(String.format("got Tag Name value \"%s\": %s",
	// getWebElement().getTagName(), locator.getBy()));
	// return getWebElement().getTagName();
	// }
	//
	// public String getAttribute(String name) {
	// logStartAction(String.format("get %s attribute: %s", name, locator.getBy()));
	// logEndAction(
	// String.format("got attribute value \"%s\": %s",
	// getWebElement().getAttribute(name), locator.getBy()));
	// return getWebElement().getAttribute(name);
	// }
	//
	// public boolean isSelected() {
	// logStartAction(String.format("get Selected status: %s", locator.getBy()));
	// logEndAction(String.format("got Selected status \"%s\": %s",
	// getWebElement().isSelected(), locator.getBy()));
	// return getWebElement().isSelected();
	// }
	//
	// public boolean isEnabled() {
	// logStartAction(String.format("get Enabled status: %s", locator.getBy()));
	// logEndAction(String.format("got Enabled status \"%s\": %s",
	// getWebElement().isEnabled(), locator.getBy()));
	// return getWebElement().isEnabled();
	// }

	public String getText(int timeoutInSeconds) {
		StopWatch sw = new StopWatch();
		sw.start();
		String result = null;

		try {
			result = this.getElement().getText();
		} catch (StaleElementReferenceException staleEx) {
			result = this.waitForElement(timeoutInSeconds - (int) sw.getTotalTimeSeconds())
					.getText(timeoutInSeconds - (int) sw.getTotalTimeSeconds());
		} catch (Exception ex) {
			_log.info(ex.getStackTrace());
		} finally {
			sw.stop();
		}

		logEndAction(String.format("Got text from: %s", this._locator.getBy()));
		return result;
	}
	
	public String getText() {
		return this.getText(this._timeoutInSeconds);
	}

	// public List<WebElement> findElements(By by) {
	// // logAction("Finding children of");
	// return getWebElement().findElements(by);
	// }
	//
	// public WebElement findElement(By by) {
	// // logAction("Finding child of");
	// return getWebElement().findElement(by);
	// }

	// public boolean isDisplayed() {
	// logStartAction(String.format("get Displayed status: %s", locator.getBy()));
	// logEndAction(String.format("got Displayed status \"%s\": %s",
	// getWebElement().isDisplayed(), locator.getBy()));
	// return getWebElement().isDisplayed();
	// }
	//
	// public Point getLocation() {
	// logStartAction(String.format("get Location: %s", locator.getBy()));
	// logEndAction(String.format("got Location \"%s\": %s",
	// getWebElement().getLocation(), locator.getBy()));
	// return getWebElement().getLocation();
	// }
	//
	// public Dimension getSize() {
	// logStartAction(String.format("get Size: %s", locator.getBy()));
	// logEndAction(String.format("got Size \"%s\": %s", getWebElement().getSize(),
	// locator.getBy()));
	// return getWebElement().getSize();
	// }
	//
	// public Rectangle getRect() {
	// return getWebElement().getRect();
	// }
	//
	// public String getCssValue(String propertyName) {
	// return getWebElement().getCssValue(propertyName);
	// }
	//
	// public void mouseTo() {
	// logStartAction(String.format("move to: %s", locator.getBy()));
	// Actions actions = new Actions(DriverManager.getDriver().getWebDriver());
	// actions.moveToElement(getWebElement()).perform();
	// logEndAction(String.format("move to: %s", locator.getBy()));
	// }

	// private void logStartAction(String msg) {
	// _log.info("Executing: {{}}", msg);
	// }

	private void logEndAction(String msg) {
		_log.info("Done: {{}}", msg);
	}

}
